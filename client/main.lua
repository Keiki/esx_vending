local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX = nil
Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while true do
		Citizen.Wait(10)

		local coords = GetEntityCoords(PlayerPedId())
		if DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 992069095, true) then
			ESX.ShowHelpNotification(_U('press_e_cola'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerServerEvent("esx_vending:buycola")
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 690372739, true) then
			ESX.ShowHelpNotification(_U('press_e_coffee'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerServerEvent("esx_vending:buycoffee")
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, -1369928609, true) then
			ESX.ShowHelpNotification(_U('press_e_slushie'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerServerEvent("esx_vending:buyslushie")
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 1243022785, true) then
			ESX.ShowHelpNotification(_U('press_e_candy'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerServerEvent("esx_vending:buycandy")
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 785076010, true) then
			ESX.ShowHelpNotification(_U('press_e_candy'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerServerEvent("esx_vending:buycandy")
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, -654402915, true) then
			ESX.ShowHelpNotification(_U('press_e_candy'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerServerEvent("esx_vending:buycandy")
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 1114264700, true) then
			ESX.ShowHelpNotification(_U('press_e_sprunk'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerServerEvent("esx_vending:buysprunk")
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, -742198632, true) then
			ESX.ShowHelpNotification(_U('press_e_water'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerEvent('esx_status:add', 'thirst', 200000)
				TriggerEvent('esx_vending:doAnim')
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 1099892058, true) then
			ESX.ShowHelpNotification(_U('press_e_buy_water'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerServerEvent("esx_vending:buywater")
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 1948359883, true) then
			ESX.ShowHelpNotification(_U('press_e_garbage'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerEvent('esx_vending:eatGarbage')
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 1098827230, true) then
			ESX.ShowHelpNotification(_U('press_e_garbage'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerEvent('esx_vending:eatGarbage')
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 897494494, true) then
			ESX.ShowHelpNotification(_U('press_e_garbage'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerEvent('esx_vending:eatGarbage')
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 1388308576, true) then
			ESX.ShowHelpNotification(_U('press_e_garbage'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerEvent('esx_vending:eatGarbage')
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 600967813, true) then
			ESX.ShowHelpNotification(_U('press_e_garbage'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerEvent('esx_vending:eatGarbage')
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 218085040, true) then
			ESX.ShowHelpNotification(_U('press_e_garbage'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerEvent('esx_vending:eatGarbage')
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 682791951, true) then
			ESX.ShowHelpNotification(_U('press_e_garbage'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerEvent('esx_vending:eatGarbage')
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, -58485588, true) then
			ESX.ShowHelpNotification(_U('press_e_garbage'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerEvent('esx_vending:eatGarbage')
			end
		elseif DoesObjectOfTypeExistAtCoords(coords.x, coords.y, coords.z, 0.8, 666561306, true) then
			ESX.ShowHelpNotification(_U('press_e_garbage'))
			if IsControlJustReleased(0, Keys['E']) and IsPedOnFoot(PlayerPedId()) then
				TriggerEvent('esx_vending:eatGarbage')
			end
		end
	end
end)

RegisterNetEvent('esx_vending:doAnim')
AddEventHandler('esx_vending:doAnim', function()
	RequestAnimDict("amb@prop_human_bum_bin@idle_b")
	while (not HasAnimDictLoaded("amb@prop_human_bum_bin@idle_b")) do Citizen.Wait(0) end
	TaskPlayAnim(PlayerPedId(),"amb@prop_human_bum_bin@idle_b","idle_d",100.0, 200.0, 0.3, 120, 0.2, 0, 0, 0)
	Wait(1000)
	StopAnimTask(PlayerPedId(), "amb@prop_human_bum_bin@idle_b","idle_d", 1.0)
end)

RegisterNetEvent('esx_vending:doVomitAnim')
AddEventHandler('esx_vending:doVomitAnim', function()
	local dict = "random@drunk_driver_1"
	local anim = "vomit_low"
	RequestAnimDict(dict)
	while (not HasAnimDictLoaded(dict)) do Citizen.Wait(0) end
	TaskPlayAnim(PlayerPedId(),dict,anim,100.0, 200.0, 0.3, 120, 0.2, 0, 0, 0)
	Wait(1000)
	StopAnimTask(PlayerPedId(), dict,anim, 1.0)
end)

RegisterNetEvent('esx_vending:eatGarbage')
AddEventHandler('esx_vending:eatGarbage', function()
	local random = math.random(1,10)

	if random == 10 then
		local playerPed  = PlayerPedId()
		TriggerEvent('esx_vending:doVomitAnim')
		Wait(1000)
		TriggerEvent('esx:showNotification', _U('food_poison'))

		while (GetEntityHealth(playerPed) > 0) do
			Citizen.Wait(1000)
			local prevHealth = GetEntityHealth(playerPed)
			local health = prevHealth
			health = health - 1

			if health ~= prevHealth then
				SetEntityHealth(playerPed, health)
				TriggerEvent('esx:showNotification', 'YOU ARE DYING OF FOOD POISONING')
			end
		end
	else
		TriggerEvent('esx_vending:doAnim')
		Wait(1000)
		TriggerEvent('esx_status:add', 'hunger', 50000)
	end
end)

RegisterNetEvent('esx_basicneeds:onDrinkeCola')
AddEventHandler('esx_basicneeds:onDrinkeCola', function(prop_name)
	if not IsAnimated then
		prop_name = prop_name or 'prop_ecola_can' --ng_proc_sodacan_01a
		IsAnimated = true

		Citizen.CreateThread(function()
			local playerPed = PlayerPedId()
			local x,y,z = table.unpack(GetEntityCoords(playerPed))
			local prop = CreateObject(GetHashKey(prop_name), x, y, z + 0.2, true, true, true)
			local boneIndex = GetPedBoneIndex(playerPed, 18905)
			AttachEntityToEntity(prop, playerPed, boneIndex, 0.12, 0.008, 0.03, 240.0, -60.0, 0.0, true, true, false, true, 1, true)

			ESX.Streaming.RequestAnimDict('mp_player_intdrink', function()
				TaskPlayAnim(playerPed, 'mp_player_intdrink', 'loop_bottle', 1.0, -1.0, 2000, 0, 1, true, true, true)

				Citizen.Wait(3000)
				IsAnimated = false
				ClearPedSecondaryTask(playerPed)
				DeleteObject(prop)
			end)
		end)

	end
end)

RegisterNetEvent('esx_basicneeds:onDrinkSprunk')
AddEventHandler('esx_basicneeds:onDrinkSprunk', function(prop_name)
	if not IsAnimated then
		prop_name = prop_name or 'prop_sprunk_can' --ng_proc_sodacan_01a
		IsAnimated = true

		Citizen.CreateThread(function()
			local playerPed = PlayerPedId()
			local x,y,z = table.unpack(GetEntityCoords(playerPed))
			local prop = CreateObject(GetHashKey(prop_name), x, y, z + 0.2, true, true, true)
			local boneIndex = GetPedBoneIndex(playerPed, 18905)
			AttachEntityToEntity(prop, playerPed, boneIndex, 0.12, 0.008, 0.03, 240.0, -60.0, 0.0, true, true, false, true, 1, true)

			ESX.Streaming.RequestAnimDict('mp_player_intdrink', function()
				TaskPlayAnim(playerPed, 'mp_player_intdrink', 'loop_bottle', 1.0, -1.0, 2000, 0, 1, true, true, true)

				Citizen.Wait(3000)
				IsAnimated = false
				ClearPedSecondaryTask(playerPed)
				DeleteObject(prop)
			end)
		end)
	end
end)

RegisterNetEvent('esx_basicneeds:onDrinkSlushie')
AddEventHandler('esx_basicneeds:onDrinkSlushie', function(prop_name)
	if not IsAnimated then
		prop_name = prop_name or 'prop_food_bs_soda_01' --ng_proc_sodacan_01a
		IsAnimated = true

		Citizen.CreateThread(function()
			local playerPed = PlayerPedId()
			local x,y,z = table.unpack(GetEntityCoords(playerPed))
			local prop = CreateObject(GetHashKey(prop_name), x, y, z + 0.2, true, true, true)
			local boneIndex = GetPedBoneIndex(playerPed, 18905)
			AttachEntityToEntity(prop, playerPed, boneIndex, 0.12, 0.008, 0.03, 240.0, -60.0, 0.0, true, true, false, true, 1, true)

			ESX.Streaming.RequestAnimDict('mp_player_intdrink', function()
				TaskPlayAnim(playerPed, 'mp_player_intdrink', 'loop_bottle', 1.0, -1.0, 2000, 0, 1, true, true, true)

				Citizen.Wait(3000)
				IsAnimated = false
				ClearPedSecondaryTask(playerPed)
				DeleteObject(prop)
			end)
		end)
	end
end)
