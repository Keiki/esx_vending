Locales['en'] = {
	['press_e_cola']    = 'press ~INPUT_PICKUP~ to buy ~g~eCola~s~',
	['press_e_sprunk']    = 'press ~INPUT_PICKUP~ to buy ~g~Sprunk~s~',
	['press_e_coffee']    = 'press ~INPUT_PICKUP~ to buy ~g~Coffee~s~',
	['press_e_slushie']    = 'press ~INPUT_PICKUP~ to buy ~g~Slushie~s~',
	['press_e_water']    = 'press ~INPUT_PICKUP~ to drink ~g~Water~s~',
	['press_e_buy_water']    = 'press ~INPUT_PICKUP~ to buy ~g~Water~s~',
	['press_e_candy']    = 'press ~INPUT_PICKUP~ to buy ~g~Candy~s~',
	['press_e_garbage']    = 'press ~INPUT_PICKUP~ to eat some ~g~Garbage~s~',
	['used_item'] = 'you used 1x ~y~%s~s~',
	['not_enough'] = 'not enough money',
	['food_poison'] = "I don't feel so good...",
}
