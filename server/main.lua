ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)


ESX.RegisterUsableItem('candy', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	local xItem =  xPlayer.getInventoryItem('candy')

	xPlayer.removeInventoryItem('candy', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 250000)
	TriggerClientEvent('esx_status:add', source, 'thirst', 30000)
	TriggerClientEvent('esx_basicneeds:onEatChips', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_item',xItem.label))
end)

ESX.RegisterUsableItem('slushie', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	local xItem =  xPlayer.getInventoryItem('slushie')

	xPlayer.removeInventoryItem('slushie', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 200000)
	TriggerClientEvent('esx_status:add', source, 'hunger', 30000)
	TriggerClientEvent('esx_basicneeds:onDrink', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_item',xItem.label))
end)

ESX.RegisterUsableItem('ecola', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	local xItem =  xPlayer.getInventoryItem('ecola')

	xPlayer.removeInventoryItem('ecola', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 200000)
	TriggerClientEvent('esx_status:add', source, 'hunger', 60000)
	TriggerClientEvent('esx_basicneeds:onDrinkeCola', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_item',xItem.label))
end)

ESX.RegisterUsableItem('sprunk', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	local xItem =  xPlayer.getInventoryItem('sprunk')

	xPlayer.removeInventoryItem('sprunk', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 200000)
	TriggerClientEvent('esx_status:add', source, 'hunger', 60000)
	TriggerClientEvent('esx_basicneeds:onDrinkSprunk', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_item',xItem.label))
end)

ESX.RegisterUsableItem('slushie', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	local xItem =  xPlayer.getInventoryItem('slushie')

	xPlayer.removeInventoryItem('slushie', 1)

	TriggerClientEvent('esx_status:add', source, 'thirst', 200000)
	TriggerClientEvent('esx_status:add', source, 'hunger', 60000)
	TriggerClientEvent('esx_basicneeds:onDrinkSlushie', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_item',xItem.label))
end)


RegisterServerEvent('esx_vending:buycola')
AddEventHandler('esx_vending:buycola', function()
	local xPlayer = ESX.GetPlayerFromId(source)
  local xItem = xPlayer.getInventoryItem('ecola')
	local price = 3 -- change this to whatever you want
	if xItem.limit ~= -1 and (xItem.count + 1) > xItem.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
	else
		if xPlayer.getMoney() > price then
			xPlayer.removeMoney(price)
			xPlayer.addInventoryItem('ecola', 1)
			TriggerClientEvent('esx_vending:doAnim', source)
		else
			TriggerClientEvent('esx:showNotification', source, _U('not_enough'))
		end
	end
end)

RegisterServerEvent('esx_vending:buysprunk')
AddEventHandler('esx_vending:buysprunk', function()
	local xPlayer = ESX.GetPlayerFromId(source)
  local xItem = xPlayer.getInventoryItem('sprunk')
	local price = 3 -- change this to whatever you want
	if xItem.limit ~= -1 and (xItem.count + 1) > xItem.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
	else
		if xPlayer.getMoney() > price then
			xPlayer.removeMoney(price)
			xPlayer.addInventoryItem('sprunk', 1)
			TriggerClientEvent('esx_vending:doAnim', source)
		else
			TriggerClientEvent('esx:showNotification', source, _U('not_enough'))
		end
	end
end)

RegisterServerEvent('esx_vending:buycoffee')
AddEventHandler('esx_vending:buycoffee', function()
	local xPlayer = ESX.GetPlayerFromId(source)
	local xItem = xPlayer.getInventoryItem('coffee')
	local price = 4 -- change this to whatever you want
	if xItem.limit ~= -1 and (xItem.count + 1) > xItem.limit then
		TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
	else
		if xPlayer.getMoney() > price then
			xPlayer.removeMoney(price)
			xPlayer.addInventoryItem('coffee', 1)
			TriggerClientEvent('esx_vending:doAnim', source)
		else
			TriggerClientEvent('esx:showNotification', source, _U('not_enough'))
		end
	end
end)

RegisterServerEvent('esx_vending:buywater')
AddEventHandler('esx_vending:buywater', function()
	local xPlayer = ESX.GetPlayerFromId(source)
	local xItem = xPlayer.getInventoryItem('water')
	local price = 2 -- change this to whatever you want
	if xItem.limit ~= -1 and (xItem.count + 1) > xItem.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
	else
		if xPlayer.getMoney() > price then
			xPlayer.removeMoney(price)
			xPlayer.addInventoryItem('water', 1)
			TriggerClientEvent('esx_vending:doAnim', source)
		else
			TriggerClientEvent('esx:showNotification', source, _U('not_enough'))
		end
	end
end)

RegisterServerEvent('esx_vending:buyslushie')
AddEventHandler('esx_vending:buyslushie', function()
	local xPlayer = ESX.GetPlayerFromId(source)
	local xItem = xPlayer.getInventoryItem('slushie')
	local price = 5 -- change this to whatever you want
	if xItem.limit ~= -1 and (xItem.count + 1) > xItem.limit then
		TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
	else
		if xPlayer.getMoney() > price then
			xPlayer.removeMoney(price)
			xPlayer.addInventoryItem('slushie', 1)
			TriggerClientEvent('esx_vending:doAnim', source)
		else
			TriggerClientEvent('esx:showNotification', source, _U('not_enough'))
		end
	end
end)

RegisterServerEvent('esx_vending:buycandy')
AddEventHandler('esx_vending:buycandy', function()
	local xPlayer = ESX.GetPlayerFromId(source)
  local xItem = xPlayer.getInventoryItem('candy')
	local price = 3 -- change this to whatever you want
	if xItem.limit ~= -1 and (xItem.count + 1) > xItem.limit then
    TriggerClientEvent('esx:showNotification', source, "Inventory is Full")
	else
		if xPlayer.getMoney() > price then
			xPlayer.removeMoney(price)
			xPlayer.addInventoryItem('candy', 1)
			TriggerClientEvent('esx_vending:doAnim', source)
		else
			TriggerClientEvent('esx:showNotification', source, _U('not_enough'))
		end
	end
end)
